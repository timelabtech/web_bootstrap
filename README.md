# README #

This project holds the web-site for Timelab.

It is the incarnation of the web-site done using Bootstrap.

The first version is from February 2016.

The full website as stored in this repo is deployed every 10 min at https://timelabdev.com/dave/web/Dave/

### GSoC Ideas page ###

The GSoC ideas page is hosted on a [Bitbucket Wiki](https://bitbucket.org/pbalm/gsoc2016/wiki/Home) and generated from there by a 
Jenkins job only accessible from within our private LAN ([private Jenkins](http://admin01:8080/)).

### Deployment ###

To deploy this web-site, you clone it locally and FTP it to the Names.co FTP server. Before FTP'ing, don't forget to enable FTP access on the [Names.co control panel](https://admin.names.co.uk/controlpanel/?domain=timelabtechnologies.com): 

- Domain Names
- Domains on Account (click timelabtechnologies.com) 
- Email and FTP
- Under Users and mailboxes click on timelabtechnologies.com 
- Under FTP Settings click the Add button (it will have the right IP filled in if your public IP has not been added yet) ([find your public IP address](https://www.google.es/webhp?q=what%20is%20my%20ip))
